# README #

This is a [Chrome extension](https://developer.chrome.com/extensions/getstarted) that gives the user the ability to add random reviewers to [BitBucket pull requests](https://www.atlassian.com/git/tutorials/making-a-pull-request/).

### What is this repository for? ###

When your BitBucket.org pull request (PR) is not getting the attention it deserves, you can now add two random reviewers to it from among the members of your team. When this is installed and you are viewing a pull request, there will be a new link called "Life is better with more eye" directly under the "Learn about pull requests" link on the right side of the PR page.