var pathParts = window.location.pathname.split('/');
var team = pathParts[1];
var repo = pathParts[2];
var prnum = pathParts[4];

members = null;
participants = null;

var fetchMembers = function (data, teamName, pageNum, callback) {
	url = 'https://api.bitbucket.org/2.0/teams/' + teamName + '/members?page=' + pageNum;
	$.getJSON(url, function (pageData) {
		if (pageData.next == null) {
			if (callback != null) {
				callback($.merge(data, pageData.values));
			}
		} else {
			fetchMembers($.merge(data, pageData.values), teamName, pageNum + 1, callback);
		}
	});
}

var fetchParticipants = function (teamName, callback) {
	url = $('#pullrequest').attr('data-participants-url').replace('!api', 'api');
	$.getJSON(url, callback);
}

var waitForTask = function (teamName, repoName, taskId, callback) {
	url = 'https://bitbucket.org/!api/1.0/repositories/' + teamName + '/' + repoName + '/pullrequests/tasks/' + taskId + '?_=' + Date.now();
	$.getJSON(url, function (data) {
		if (data.complete == true) {
			callback(data.url);
			return;
		}
		waitForTask(teamName, repoName, taskId, callback);
	});
};

var mapUsernames = function (users) {
	return $.map(users, function (user) {return user.username;});
};

var setDiff = function (a, b) {
	return $.grep(a, function (x) {
		return $.inArray(x, b) < 0;
	});
};

function shuffle (array) {
  // Found at: http://stackoverflow.com/a/2450976/823470
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

var getUserForUsername = function (users, username) {
	matches = users.filter(function (u) {
		return u.username == username;
	});
	if (matches.length > 0) {
		return matches[0];
	}
	return null;
};

var getCsrfToken = function () {
	csrfToken = $('#image-upload-template').text().match("name='csrfmiddlewaretoken' value='(.*?)'");
	if (csrfToken.length < 2) {
		alert('Content script out of date, cannot location CSRF token.');
		return;
	}
	return csrfToken[1];
};

var gotoViaPost = function (url, data) {
	form = $('<form action="' + url + '" method="post"></form>');
	csrfToken = getCsrfToken();
	$('<input>').attr('type', 'hidden').attr('name', 'csrfmiddlewaretoken').attr('value', csrfToken).appendTo(form);
	for (key in data) {
		$('<input>').attr('type', 'hidden').attr('name', key).attr('value', data[key]).appendTo(form);
	}
	$('body').append(form);
	form.submit();
};

var setPrReviewers = function (teamName, repoName, prNumber, users, callback) {
	url = 'https://bitbucket.org/' + teamName + '/' + repoName + '/pull-requests/update/' + prNumber;
	reviewersStr = mapUsernames(users).join(',');
	sourceBranch = $('#pullrequest').attr('data-source-branch');
	data = {
		reviewers: reviewersStr,
		dest: $('#pullrequest').attr('data-compare-dest-value'),
		source: team + '/' + repoName + ':' + $('#pullrequest').attr('data-source-cset').substring(0, 12) + ':' + sourceBranch,
		title: $('meta[name="og:title"]').attr('content'),
		description: $('meta[name="og:description"]').attr('content'),
		close_anchor_branch: 'on',
		csrfmiddlewaretoken: getCsrfToken()
	};
	$.post(url, data, callback);
}

var moreeye = function (e) {
	work = function (members, participants) {
		notHere = shuffle(setDiff(mapUsernames(members), mapUsernames(participants)));
		if (notHere.length == 0) {
			alert('The whole company is already on this PR - you probably have enough publicity already!');
			return;
		}
		randomUsernames = notHere.slice(0, 2);
		randomUsers = $.map(randomUsernames, function (username) {return getUserForUsername(members, username);})
		randomDisplayNames = $.map(randomUsers, function (user) {return user.display_name;});
		
		if (confirm('Random reviewers will be ' + randomDisplayNames.join(' & ') + ', is that okay?') == true) {
			author = JSON.parse($('body').attr('data-current-pr')).author.username;
			newParticipants = jQuery.grep($.merge(participants.slice(), randomUsers), function (v) {
				return v.username != author;
			});
			setPrReviewers(team, repo, prnum, newParticipants, function (x) {
				waitForTask(team, repo, x.taskId, function (newUrl) {
					location.href = newUrl;
				})
			});
		}
	};
	
	if (members != null && participants != null) {
		work(members, participants);
	} else {
		fetchMembers([], team, 1, function (ms) {
			fetchParticipants(team, function (ps) {
				members = ms;
				participants = ps;
				work(members, participants);
			});
		});
	}
}


$('.detail-summary--section').append('<li class="detail-summary--item"><span class="aui-icon aui-icon-small aui-iconfont-devtools-compare detail-summary--icon"></span><a id="moreeye">Life is better with more eye</a></li>');

$('#moreeye').css('cursor', 'pointer');
$('#moreeye').click(moreeye);
